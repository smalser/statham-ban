from setuptools import setup


setup(
    name='statham',
    version='0.1',
    description='Запрещает',
    install_requires=[
        'Pillow==8.4.0'
    ],
    packages=['statham'],
    package_dir={'statham': 'statham'},
    package_data={'statham': ['font.ttf', 'template.jpg']},
)
