import os
import sys
from io import BytesIO

from PIL import Image, ImageFont, ImageDraw
import statham

default_size = 90
size = default_size
startpos = (40, 470)
endpos = (475, 970)
dpos = (435, 500)

if not "__path__" in globals():
    if not "__path__" in statham.__dict__:
        __path__ = [""]
    __path__ = statham.__path__

def findcenter(text: str):
    text_size = draw.multiline_textsize(text, font=font)
    delta = dpos[0] - text_size[0]
    return (startpos[0] + int(delta/2), startpos[1])

def choose_size(txt):
    global font, size
    while draw.multiline_textsize(txt, font=font)[0] > dpos[0] or draw.multiline_textsize(txt, font=font)[1] > dpos[1]:
        size -= 2
        print(f"changing size to: {size}")
        font = ImageFont.truetype(os.path.join(__path__[0], "./font.ttf") , size=size)

def find_best_place(txt):
    raznica = {}
    lines = len(txt)
    if lines <= 4:
        return "\n".join(txt)
    
    for l1 in range(1, lines-2):
        for l2 in range(l1+1, lines-1):
            raznica[abs(len(txt[:l1])-len(txt[l1:l2])) + abs(len(txt[:l1])-len(txt[l2:])) + abs(len(txt[l1:l2])-len(txt[l2:]))] = " ".join(txt[:l1]) + "\n" + " ".join(txt[l1:l2]) + "\n" + " ".join(txt[l2:])
            for l3 in range(l2+1, lines):
                a = abs(len(txt[:l1])-len(txt[l1:l2])) + abs(len(txt[:l1])-len(txt[l2:l3])) + abs(len(txt[l1:l2])-len(txt[l2:l3])) + abs(len(txt[:l1])-len(txt[l3:])) + abs(len(txt[l3:])-len(txt[l2:l3])) + abs(len(txt[l1:l2])-len(txt[l3:]))
                raznica[a] = " ".join(txt[:l1]) + "\n" + " ".join(txt[l1:l2]) + "\n" + " ".join(txt[l2:l3]) + "\n" + " ".join(txt[l3:])
                
    minkey = min(raznica.keys())
    return raznica[minkey]
        
    

def drop_for_lines(txt: str) -> str:
    if draw.multiline_textsize(txt, font=font)[0] > dpos[0]:
        spl = txt.split(" ")
        if len(spl) > 1:  # Пробуем сплитить
            txt = find_best_place(spl)
    return txt
       
def ban(text, save: bool = False):
    global img, draw, font, size
    size = default_size
    img = Image.open(os.path.join(__path__[0],"template.jpg"))
    draw = ImageDraw.Draw(img)
    font = ImageFont.truetype(os.path.join(__path__[0], "./font.ttf") , size=default_size)

    #  На сплит
    if not "\n" in text:
        txt = drop_for_lines(text)
    else:
        txt = text
        
    #  На размер
    choose_size(txt)

    draw.multiline_text(findcenter(txt), txt, (0,0,0),font=font, align="center")
    if save:
        img.save("banned.jpg", format='jpeg')
    filestream = BytesIO()
    img.save(filestream, format='jpeg')
    filestream.seek(0)
    return filestream

if __name__ == "__main__":
    text = "\n".join(sys.argv[1].split(r"\n"))
    fs = ban(text)
    with open("banned.jpg", "wb") as f:
      f.write(fs.read())  